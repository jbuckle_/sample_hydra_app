# A secret token used to encrypt user_id's in the Bookmarks#export callback URL
# functionality, for example in Refworks export of Bookmarks. In Rails 4, Blacklight
# will use the application's secret key base instead.
#

# Blacklight.secret_key = '6cc447b399b18b84ecc909df01d965833e67f324e8591deb52b8d7829cb95f661780a3ef0641a831563c211c917d2e9135735dbb6b9966847711cd04d12a4fc6'

